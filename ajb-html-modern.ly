\version "2.18.2"
\language "english"
\layout {
  indent = #-3
  short-indent = #-3
  \context {
    \Score
    \remove "Bar_number_engraver"
    \override SpacingSpanner.common-shortest-duration = #(ly:make-moment 3 16)
  }
  \context {
    \Staff
    \remove Time_signature_engraver
  }
}

