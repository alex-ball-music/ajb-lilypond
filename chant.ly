\version "2.14.2"
\include "english.ly"

\layout {
 indent = #0
 short-indent = #0
 \context {
  \Score
  autoAccidentals = #`(Staff ,(make-accidental-rule 'same-octave 0)
    ,(make-accidental-rule 'any-octave 0)
    ,(make-accidental-rule 'same-octave 1))
  \remove "Bar_number_engraver"
  timing = ##t
  \override SpacingSpanner
   #'common-shortest-duration = #(ly:make-moment 1 2)
 }
 \context {
  \Staff
  \remove Time_signature_engraver
 }
}
