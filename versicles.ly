\version "2.14.2"
\include "english.ly"

\layout {
 indent = #0
 short-indent = #0
 \context {
  \Score
  \remove "Bar_number_engraver"
  timing = ##f
  \override SpacingSpanner
   #'common-shortest-duration = #(ly:make-moment 1 2)
 }
 \context {
  \Staff
  \remove Time_signature_engraver
  \override VerticalAxisGroup
  #'minimum-Y-extent = #'(-3 . 4)
 }
 \context {
  \Lyrics
  \override StanzaNumber #'font-name = #"Alegreya"
  \override StanzaNumber #'font-size = #2.8 % Utopia = 2.1
  \override LyricText #'font-name = #"Alegreya"
  \override LyricText #'font-size = #2.8 % Utopia = 2.1
  \override VerticalAxisGroup
  #'minimum-Y-extent = #'(-0 . 0)
 }
}

dropLyrics =
{
 \override LyricText #'extra-offset = #'(0 . -1)
 \override LyricHyphen #'extra-offset = #'(0 . -1)
 \override LyricExtender #'extra-offset = #'(0 . -1)
}
raiseLyrics =
{
 \revert LyricText #'extra-offset
 \revert LyricHyphen #'extra-offset
 \revert LyricExtender #'extra-offset
}

% Stock functions from bar-line.scm
#(define (calc-blot thickness extent grob)
  "Calculate the blot diameter by taking @code{'rounded}
and the dimensions of the extent into account."
  (let* ((rounded (ly:grob-property grob 'rounded #f))
         (blot (if rounded
                   (let ((blot-diameter (layout-blot-diameter grob))
                         (height (interval-length extent)))

                     (cond ((< thickness blot-diameter) thickness)
                           ((< height blot-diameter) height)
                           (else blot-diameter)))
                   0)))

    blot))
#(define (staff-symbol-line-count staff)
  "Get or compute the number of lines of staff @var{staff}."
  (let ((line-count 0))

    (if (ly:grob? staff)
        (let ((line-pos (ly:grob-property staff 'line-positions '())))

          (set! line-count (if (pair? line-pos)
                               (length line-pos)
                               (ly:grob-property staff 'line-count 0)))))

    line-count))
#(define (get-staff-symbol grob)
  "Return the staff symbol corresponding to Grob @var{grob}."
  (if (grob::has-interface grob 'staff-symbol-interface)
      grob
      (ly:grob-object grob 'staff-symbol)))

% New function
#(define (make-middle-bar-line grob extent)
  "Draw a two-thirds bar line."
  (let* ((line-thickness (layout-line-thickness grob))
         (thickness (* (ly:grob-property grob 'hair-thickness 1)
                       line-thickness))
         (half-height (* 1/2 (+ (interval-start extent) (interval-end extent))))
         (staff-space (ly:staff-symbol-staff-space grob))
         (staff-symbol (get-staff-symbol grob))
         (gap-count (- (staff-symbol-line-count staff-symbol) 1))
         (third-height (* 1/3 (* staff-space gap-count)))
         (blot (calc-blot thickness extent grob)))

    (ly:round-filled-box (cons 0 thickness)
                         (cons (- half-height third-height) (+ half-height third-height))
                         blot)))
% Registration of new function
#(add-bar-glyph-print-procedure "," make-middle-bar-line)
#(define-bar-line "," "," #f #f)

