# Alex Ball’s ‘house style’ for Lilypond

This repository contains files I use for applying a uniform style to music
typeset using [Lilypond](http://lilypond.org/).

## Lilypond scripts aimed at full scores

### Lilypond 2.20

`ajb-common.ly` is an all-in-one setup for scores:

```.ly
\version "2.20.0"
\language "english"
theStaffSize = 18
\include "ajb-common.ly"
```

You can omit the `theStaffSize` line; it defaults to 19.

Functions provided for lyrics:

  - `\unSlur` for ignoring a slur for the current verse, reverted
    afterwards with `\reSlur`.

  - `\sp` for skipping a note in the current verse.

  - `\all` makes lyrics bold, reverted with `\cantor`.

  - `\melis` aligns the next syllable left, useful for notes that are tied or
    slurred in a voice other than the one tied to the lyrics.

  - `\dropLyrics` and `\raiseLyrics` move lyrics down or up half a line,
    respectively – useful for splitting/joining lyric lines (for alternative
    passages or stanzas with shared passages); undone with `\revertLyrics`.

Other functions

  - `\splitStaffBarLine` breaks the current line and adds arrows to the end
    to indicate the staff will split in two in the next system.

  - `\convDownStaffBarLine` and `\convUpStaffBarLine` break the current line and
    add an arrow to the end to indicate the staff will join with the one below
    or above, respectively.

### Lilypond 2.18

`ajb-setup.ly` provides some additional functions for

   - setting up a font tree with an additional scaling factor, allowing you to
     scale the text universally with respect to the staff size (useful for
     scaling down fonts with large x-heights) – this relies on patching one of
     the core files (see below);

   - merging rests that occur simultaneously in multiple voices – switched on
     with `\mergeRests` in Staff context;

   - suppressing the automated tagline while allowing custom ones;

   - convenience commands for entering lyrics:

      * `\unSlur` for ignoring a slur for the current verse, reverted afterwards
        with `\reSlur`;

      * `\sp` for skipping a note in the current verse;

      * `\dropLyrics` and `\raiseLyrics` move lyrics down or up half a line,
        respectively – useful for splitting/joining lyric lines (for alternative
        passages or stanzas with shared passages);

      * `\revertLyrics` for undoing the above.

For the above font settings to work, a line in the core file `scm/font.scm` has
to be changed from this:

```scheme
(define (make-font-tree-node
```

to this:

```scheme
(define-public (make-font-tree-node
```

`ajb-paper.ly` applies my preferences for page margins, headers, footers and the
title block, including font choices. You must define a value for `theStaffSize`
(e.g. 18) before calling this snippet.

`ajb-layout.ly` applies my preferences for the musical layout, such as italic
bar numbers, merged rests, and straight quaver flags (useful for choral music
where you would want to shorten quaver stems to avoid collisions with lyrics).

In a Lilypond score, you would normally load these at the start, like so:

```.ly
\version "2.18.2"
\language "english"
\include "ajb-setup.ly"
theStaffSize = #18
\include "ajb-paper.ly"
\include "ajb-layout.ly"
```

## Lilypond scripts aimed at lilypond-book snippets

`ajb-chant.ly` provides a layout for Anglican chant.

`ajb-chanting.ly` provides a layout and functions supporting modern notation for
Gregorian chant:

  - `\all` switches lyrics to a bold font;

  - `\cantor` switches lyrics (back) to a normal weight font;

  - `\dropLyrics` and `\raiseLyrics` move lyrics down or up half a line,
    respectively – useful for stacking long recitations onto two lines, or
    splitting/joining lyric lines (for alternative passages or stanzas with
    shared passages);

  - `\revertLyrics` for undoing the above;

  - `\bar"'"` for a modern *divisio minima*;

  - `\bar","` for a modern *divisio maior*;

  - `\pLeft` and `\pRight` for parenthesizing one or more notes, using a more
    generous size than you get with the standard one-note function.

## Obsolete Lilypond scripts I used to use

I keep these around for the sake of older pieces that use them.

`bookscripts.ly` is the forerunner of `ajb-setup.ly`.

`chant.ly` is the forerunner of `ajb-chant.ly`.

`responses.ly` and `versicles.ly` were combined and extended to make
`ajb-chanting.ly`.

## Installing the scripts

`customize-ly` is a Bash script for making these scripts available to all
Lilypond scores and snippets on a Unix-like system. It installs them as symlinks
so changes here are reflected immediately, and it also patches the Scheme
`make-font-tree-node` function.

If you run it with no arguments, it will attempt to do all this within the
alphabetically last directory in `/usr/share/lilypond`. To install to a
different directory, specify it as the script's first argument.

If the installation is not writeable by the current user, the script will
automatically call `sudo`.
