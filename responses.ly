\version "2.14.2"
\include "english.ly"

\layout {
 indent = #0
 short-indent = #0
 \context {
  \Score
  \remove "Bar_number_engraver"
  timing = ##f
  \override SpacingSpanner
   #'common-shortest-duration = #(ly:make-moment 1 2)
 }
 \context {
  \Staff
  \remove Time_signature_engraver
 }
 \context {
  \Lyrics
  \override LyricText #'font-name = #"Alegreya Bold"
  \override LyricText #'font-size = #2.8 % Utopia = 2.1
  \override VerticalAxisGroup
  #'minimum-Y-extent = #'(-0 . 0)
 }
}

dropLyrics =
{
 \override LyricText #'extra-offset = #'(0 . -1)
 \override LyricHyphen #'extra-offset = #'(0 . -1)
 \override LyricExtender #'extra-offset = #'(0 . -1)
}
raiseLyrics =
{
 \revert LyricText #'extra-offset
 \revert LyricHyphen #'extra-offset
 \revert LyricExtender #'extra-offset
}
