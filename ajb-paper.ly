\version "2.18.0"
% You must set theStaffSize before calling this snippet
% theStaffSize = #20
#(set-global-staff-size theStaffSize)

\paper {
  %#(define fonts (make-scaled-pango-font-tree "Noto Serif" "Noto Sans" "Droideka Mono" 0.9 (/ theStaffSize 20)))
  %#(define fonts (make-scaled-pango-font-tree "Alegreya" "Alegreya Sans" "Lekton" 1.1 (/ theStaffSize 20)))
  %#(define fonts (make-pango-font-tree "Andada" "Alegreya Sans" "Lekton" (/ theStaffSize 20)))
  #(define fonts (make-pango-font-tree "Source Serif Pro" "Source Sans Pro" "Ricty Diminished" (/ theStaffSize 20)))
  %#(define fonts (make-pango-font-tree "Charis SIL" "Source Sans Pro" "Ricty Diminished" (/ theStaffSize 20)))
  %#(define fonts (make-scaled-pango-font-tree "Merriweather" "Merriweather Sans" "Roboto Mono" 0.9 (/ theStaffSize 20)))
  %#(define fonts (make-scaled-pango-font-tree "Vollkorn" "Cronos Pro" "Lekton" 1.05 (/ theStaffSize 20)))
  bookTitleMarkup = \markup {
    \override #'(baseline-skip . 3.5)
    \large \column {
      \override #'(baseline-skip . 3.5)
      \column {
        \fill-line {
          \italic \fromproperty #'header:dedication
        }
        \vspace #0.5
        \fontsize #2 \larger \bold
        \fill-line {
          \larger \sans \fromproperty #'header:title
        }
        \fill-line {
          \fontsize #1 \smaller \bold
          \larger \sans \fromproperty #'header:subtitle
        }
        \fill-line {
          \smaller \sans \bold
          \fromproperty #'header:subsubtitle
        }
        \fill-line {
          \fromproperty #'header:poet
          { \fontsize #1 \bold \fromproperty #'header:instrument }
          \fromproperty #'header:composer
        }
        \fill-line {
          \fromproperty #'header:meter
          \fromproperty #'header:arranger
        }
      }
    }
  }
  scoreTitleMarkup = \markup {
    \column {
      \on-the-fly \print-all-headers { \bookTitleMarkup \hspace #1 }
      \fill-line {
        { \fontsize #2 \fromproperty #'header:piece }
        \fromproperty #'header:opus
      }
    }
  }
  oddHeaderMarkup = \markup {
    \fill-line {
      \on-the-fly #not-first-page \fromproperty #'header:instrument
    }
  }
  evenHeaderMarkup = \oddHeaderMarkup
  oddFooterMarkup = \markup {
    \vspace #1
    \column {
      \fill-line {
        %% Copyright header field only on first page.
        \on-the-fly #first-page \sans \fromproperty #'header:copyright
      }
      \on-the-fly #first-page { \on-the-fly #last-page \vspace #1 }
      \fill-line {
        %% Tagline header field only on last page.
        \on-the-fly #last-page \sans \fromproperty #'header:tagline
      }
      \fill-line {
        %% Page numbers on all pages (if turned on) except the last
        \on-the-fly #print-page-number-check-first
        \on-the-fly #not-last-page \sans \fromproperty #'page:page-number-string
      }
      \fill-line {
        %% Page number (if turned on) on last page iff there's no tagline
        \on-the-fly #print-page-number-check-first
        \on-the-fly #last-page
        \on-the-fly #no-tagline
        \sans \fromproperty #'page:page-number-string
      }
    }
  }
  ragged-bottom = ##f
  top-margin = 10
  left-margin = 15
  right-margin = 15
  bottom-margin = 10
  last-bottom-spacing #'basic-distance = #0
  markup-system-spacing #'padding = #2
  %  line-width = 18\cm
  %  head-separation = 0.2\cm
  %  page-top-space = 0.6\cm
  %  after-title-space = 1.5\cm
%%% Other handy declarations:
  %  between-system-padding = #0.1
  %  between-system-space = #0.1
  %  ragged-last-bottom = ##f
  %  system-count = #6
}


%{
convert-ly (GNU LilyPond) 2.18.2  convert-ly: Processing `'...
Applying conversion: 2.17.0, 2.17.4, 2.17.5, 2.17.6, 2.17.11, 2.17.14,
2.17.15, 2.17.18, 2.17.19, 2.17.20, 2.17.25, 2.17.27, 2.17.29,
2.17.97, 2.18.0
%}
