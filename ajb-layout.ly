\version "2.18.0"
\layout {
  \context { \Score
    \override BarNumber.font-shape = #'italic
    \override BarNumber.padding = #2.0
    \override RehearsalMark.font-size = #1
  }
  \context { \Staff
    \RemoveEmptyStaves
    \mergeRests
    \override VerticalAxisGroup.remove-first = ##t
    \override TimeSignature.style = #'()
  }
  \context { \Voice
    \override Flag.stencil = #modern-straight-flag
    \override DynamicTextSpanner.font-size = #0
  }
  \context { \Lyrics
    \override LyricText.font-size = #0
    \override StanzaNumber.font-size = #0
    \override LyricSpace.minimum-distance = #1.0
  }
  \context { \Dynamics
    \override DynamicTextSpanner.font-size = #0
  }
}


%{
convert-ly (GNU LilyPond) 2.18.2  convert-ly: Processing `'...
Applying conversion: 2.17.0, 2.17.4, 2.17.5, 2.17.6, 2.17.11, 2.17.14,
2.17.15, 2.17.18, 2.17.19, 2.17.20, 2.17.25, 2.17.27, 2.17.29,
2.17.97, 2.18.0
%}
