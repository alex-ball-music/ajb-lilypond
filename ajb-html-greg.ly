\version "2.18.2"
\include "ajb-chanting.ly"
\layout {
  indent = #-3
  short-indent = #-3
  \context {
    \Score
    \override SpacingSpanner.common-shortest-duration = #(ly:make-moment 3 16)
  }
  \context {
    \Staff
    \hide Stem
    \override Stem #'length = #0
  }
  \context {
    \Voice
    \consists "Horizontal_bracket_engraver"
    \override HorizontalBracket.direction = #UP
  }
}