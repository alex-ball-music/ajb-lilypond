\version "2.24.0"

%%% Useful tests for footers %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#(define (no-tagline layout props arg)
  (if (not (chain-assoc-get 'header:tagline props #f))
      (interpret-markup layout props arg)
      empty-stencil))
#(define (book-last-page? layout props)
   "Return #t iff the current page number, got from @code{props}, is the
book last one."
   (and (chain-assoc-get 'page:is-bookpart-last-page props #f)
        (chain-assoc-get 'page:is-last-bookpart props #f)))
#(define (not-last-page layout props arg)
  (if (not (book-last-page? layout props))
      (interpret-markup layout props arg)
      empty-stencil))

%%% Shorthands %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#(define-markup-command (smcp layout props text) (markup?)
  "Uses true small caps for text."
  (interpret-markup layout props
    (markup #:override '(font-features . ("smcp")) text)))

unSlur = {
  \set ignoreMelismata = ##t
}

reSlur = {
  \unset ignoreMelismata
}

sh = {
  \skip4
}

all = \override Lyrics.LyricText.font-series = #'bold

cantor = \override Lyrics.LyricText.font-series = #'medium

melis = \once \override LyricText.self-alignment-X = #LEFT

dropLyrics =
{
 \override StanzaNumber.extra-offset = #'(0 . -1)
 \override LyricText.extra-offset = #'(0 . -1)
 \override LyricHyphen.extra-offset = #'(0 . -1)
 \override LyricExtender.extra-offset = #'(0 . -1)
}

raiseLyrics =
{
 \override StanzaNumber.extra-offset = #'(0 . 1)
 \override LyricText.extra-offset = #'(0 . 1)
 \override LyricHyphen.extra-offset = #'(0 . 1)
 \override LyricExtender.extra-offset = #'(0 . 1)
}

revertLyrics =
{
 \revert StanzaNumber.extra-offset
 \revert LyricText.extra-offset
 \revert LyricHyphen.extra-offset
 \revert LyricExtender.extra-offset
}

#(define-markup-command (arrow-at-angle layout props angle-deg length fill)
   (number? number? boolean?)
   (let* (
           (PI-OVER-180 (/ (atan 1 1) 34))
           (degrees->radians (lambda (degrees) (* degrees PI-OVER-180)))
           (angle-rad (degrees->radians angle-deg))
           (target-x (* length (cos angle-rad)))
           (target-y (* length (sin angle-rad))))
     (interpret-markup layout props
       (markup
        #:translate (cons (/ target-x 2) (/ target-y 2))
        #:rotate angle-deg
        #:translate (cons (/ length -2) 0)
        #:concat (#:draw-line (cons length 0)
                   #:arrow-head X RIGHT fill)))))


splitStaffBarLineMarkup = \markup \with-dimensions #'(0 . 0) #'(0 . 0) {
  \combine
  \arrow-at-angle #45 #(sqrt 8) ##t
  \arrow-at-angle #-45 #(sqrt 8) ##t
}

splitStaffBarLine = {
  \once \override Staff.BarLine.stencil =
  #(lambda (grob)
     (ly:stencil-combine-at-edge
      (ly:bar-line::print grob)
      X RIGHT
      (grob-interpret-markup grob splitStaffBarLineMarkup)
      0))
  \break
}

convDownStaffBarLine = {
  \once \override Staff.BarLine.stencil =
  #(lambda (grob)
     (ly:stencil-combine-at-edge
      (ly:bar-line::print grob)
      X RIGHT
      (grob-interpret-markup grob #{
        \markup\with-dimensions #'(0 . 0) #'(0 . 0) {
          \translate #'(0 . -.13)\arrow-at-angle #-45 #(sqrt 8) ##t
        }#})
      0))
  \break
}

convUpStaffBarLine = {
  \once \override Staff.BarLine.stencil =
  #(lambda (grob)
     (ly:stencil-combine-at-edge
      (ly:bar-line::print grob)
      X RIGHT
      (grob-interpret-markup grob #{
        \markup\with-dimensions #'(0 . 0) #'(0 . 0) {
          \translate #'(0 . .14)\arrow-at-angle #45 #(sqrt 8) ##t
        }#})
      0))
  \break
}

%%% Paper settings %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#(set-global-staff-size (if (defined? 'theStaffSize) theStaffSize 19))

\paper {
  #(define fonts
    (make-pango-font-tree "Source Serif Pro"
                          "Source Sans Pro"
                          "Ricty Diminished"
                          (/ staff-height pt 20)))

  #(set-paper-size "a4")

  bookTitleMarkup = \markup {
    \override #'(baseline-skip . 3.5)
    \large \column {
      \override #'(baseline-skip . 3.5)
      \column {
        \fill-line {
          \italic \fromproperty #'header:dedication
        }
        \vspace #0.5
        \fontsize #2 \larger \bold
        \fill-line {
          \larger \sans \fromproperty #'header:title
        }
        \fill-line {
          \fontsize #1 \smaller \bold
          \larger \sans \fromproperty #'header:subtitle
        }
        \fill-line {
          \smaller \sans \bold
          \fromproperty #'header:subsubtitle
        }
        \fill-line {
          \fromproperty #'header:poet
          { \fontsize #1 \bold \sans \fromproperty #'header:instrument }
          \fromproperty #'header:composer
        }
        \fill-line {
          \fromproperty #'header:meter
          \fromproperty #'header:arranger
        }
      }
    }
  }
  scoreTitleMarkup = \markup {
    \column {
      \if \should-print-all-headers { \bookTitleMarkup \hspace #1 }
      \fill-line {
        { \fontsize #2 \fromproperty #'header:piece }
        \fromproperty #'header:opus
      }
    }
  }
  oddHeaderMarkup = \markup {
    \fill-line {
      \unless \on-first-page \fromproperty #'header:instrument
    }
  }
  evenHeaderMarkup = \oddHeaderMarkup
  oddFooterMarkup = \markup {
    \vspace #1
    \column {
      \fill-line {
        %% Copyright header field only on first page.
        \if \on-first-page \sans \fromproperty #'header:copyright
      }
      \if \on-first-page { \if \on-last-page \vspace #1 }
      \fill-line {
        %% Tagline header field only on last page.
        \if \on-last-page \sans \fromproperty #'header:tagline
      }
      \fill-line {
        %% Page numbers on all pages (if turned on) except the last
        \if \should-print-page-number
        \on-the-fly #not-last-page \sans \fromproperty #'page:page-number-string
      }
      \fill-line {
        %% Page number (if turned on) on last page iff there's no tagline
        \if \should-print-page-number
        \if \on-last-page
        \on-the-fly #no-tagline
        \sans \fromproperty #'page:page-number-string
      }
    }
  }
  ragged-bottom = ##f
  top-margin = 10
  left-margin = 15
  right-margin = 15
  bottom-margin = 10
  last-bottom-spacing.basic-distance = #0
  markup-system-spacing.padding = #2
  %  line-width = 18\cm
  %  head-separation = 0.2\cm
  %  page-top-space = 0.6\cm
  %  after-title-space = 1.5\cm
%%% Other handy declarations:
  %  between-system-padding = #0.1
  %  between-system-space = #0.1
  %  ragged-last-bottom = ##f
  %  system-count = #6
}

%%% Layout defaults %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\layout {
  \context { \Score
    \override BarNumber.font-shape = #'italic
    \override BarNumber.padding = #2.0
    \override RehearsalMark.font-size = #1
  }
  \context { \Staff
    \consists "Merge_rests_engraver"
    \RemoveAllEmptyStaves
    \numericTimeSignature
  }
  \context { \Voice
    \consists "Melody_engraver"
    \override Flag.stencil = #modern-straight-flag
    \override DynamicTextSpanner.font-size = #0
    \override Stem.neutral-direction = #'()
  }
  \context { \Lyrics
    \override LyricText.font-size = #0
    \override StanzaNumber.font-size = #0
    \override LyricSpace.minimum-distance = #1.0
  }
  \context { \Dynamics
    \override DynamicTextSpanner.font-size = #0
  }
}
