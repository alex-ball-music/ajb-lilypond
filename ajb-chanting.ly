\version "2.18.2"
\language "english"

%%% Layout

\layout {
 indent = #0
 short-indent = #0
 \context {
  \Score
  \remove "Bar_number_engraver"
  timing = ##f
  \override SpacingSpanner.common-shortest-duration = #(ly:make-moment 1 2)
 }
 \context {
  \Staff
  \remove Time_signature_engraver
  \accidentalStyle neo-modern
  \override VerticalAxisGroup.minimum-Y-extent = #'(-3 . 4)
  \override ParenthesesItem.font-size = #2
 }
 \context {
  \Lyrics
  % Fonts are sized to give 10pt text at staffsize 14
  % Alegreya = 2.8; Utopia = 2.1
  \override StanzaNumber.font-size = #2.8
  \override LyricText.font-size = #2.8
  \override VerticalAxisGroup.minimum-Y-extent = #'(-0 . 0)
 }
}

%%% Functions to change lyrics fonts

all = \override Lyrics.LyricText.font-series = #'bold
cantor = \override Lyrics.LyricText.font-series = #'medium

%%% Functions to adjust position of lyrics

dropLyrics =
{
 \override LyricText.extra-offset = #'(0 . -1)
 \override LyricHyphen.extra-offset = #'(0 . -1)
 \override LyricExtender.extra-offset = #'(0 . -1)
}
raiseLyrics =
{
 \override LyricText.extra-offset = #'(0 . 1)
 \override LyricHyphen.extra-offset = #'(0 . 1)
 \override LyricExtender.extra-offset = #'(0 . 1)
}
revertLyrics =
{
 \revert LyricText.extra-offset
 \revert LyricHyphen.extra-offset
 \revert LyricExtender.extra-offset
}
recite = \once \override LyricText.self-alignment-X = #LEFT

%%% Extra barline functions

% Stock functions from bar-line.scm
#(define (calc-blot thickness extent grob)
  "Calculate the blot diameter by taking @code{'rounded}
and the dimensions of the extent into account."
  (let* ((rounded (ly:grob-property grob 'rounded #f))
         (blot (if rounded
                   (let ((blot-diameter (layout-blot-diameter grob))
                         (height (interval-length extent)))

                     (cond ((< thickness blot-diameter) thickness)
                           ((< height blot-diameter) height)
                           (else blot-diameter)))
                   0)))

    blot))
#(define (staff-symbol-line-count staff)
  "Get or compute the number of lines of staff @var{staff}."
  (let ((line-count 0))

    (if (ly:grob? staff)
        (let ((line-pos (ly:grob-property staff 'line-positions '())))

          (set! line-count (if (pair? line-pos)
                               (length line-pos)
                               (ly:grob-property staff 'line-count 0)))))

    line-count))
#(define (get-staff-symbol grob)
  "Return the staff symbol corresponding to Grob @var{grob}."
  (if (grob::has-interface grob 'staff-symbol-interface)
      grob
      (ly:grob-object grob 'staff-symbol)))

% Replacement for 'tick' bar line, slightly thicker
#(define (make-tick-bar-line grob extent)
  "Draw a tick bar line."
  (let* ((half-tick (* 2/3 (ly:staff-symbol-staff-space grob)))
         (line-thickness (layout-line-thickness grob))
         (thickness (* (ly:grob-property grob 'hair-thickness 1)
                       line-thickness))
         (height (interval-end extent))
         (blot (calc-blot thickness extent grob)))

    (ly:round-filled-box (cons 0 thickness)
                         (cons (- height half-tick) (+ height half-tick))
                         blot)))
#(add-bar-glyph-print-procedure "'" make-tick-bar-line)
#(define-bar-line "'" "'" #f #f)

% New function for 'middle' bar line, with gaps top and bottom
#(define (make-middle-bar-line grob extent)
  "Draw a two-thirds bar line."
  (let* ((line-thickness (layout-line-thickness grob))
         (thickness (* (ly:grob-property grob 'hair-thickness 1)
                       line-thickness))
         (half-height (* 1/2 (+ (interval-start extent) (interval-end extent))))
         (staff-space (ly:staff-symbol-staff-space grob))
         (staff-symbol (get-staff-symbol grob))
         (gap-count (- (staff-symbol-line-count staff-symbol) 1))
         (third-height (* 1/3 (* staff-space gap-count)))
         (blot (calc-blot thickness extent grob)))

    (ly:round-filled-box (cons 0 thickness)
                         (cons (- half-height third-height) (+ half-height third-height))
                         blot)))
#(add-bar-glyph-print-procedure "," make-middle-bar-line)
#(define-bar-line "," "," #f #f)

%%% Functions to aid parenthesizing notes

#(define-public
  (parentheses-item::calc-parenthesis-left-stencils grob)
  (let* ((font (ly:grob-default-font grob))
         (lp (ly:font-get-glyph font "accidentals.leftparen")))
  (list lp empty-stencil)))

#(define-public
  (parentheses-item::calc-parenthesis-right-stencils grob)
  (let* ((font (ly:grob-default-font grob))
         (rp (ly:font-get-glyph font "accidentals.rightparen")))
  (list empty-stencil rp)))

pLeft = \once \override ParenthesesItem.stencils = #parentheses-item::calc-parenthesis-left-stencils

pRight = \once \override ParenthesesItem.stencils = #parentheses-item::calc-parenthesis-right-stencils
